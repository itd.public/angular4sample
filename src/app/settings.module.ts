/**
 * Created by Kiem on 07/11/2017.
 */
import * as $ from 'jquery';
export class SettingModule {
    private status_left = false;
    private status_right = false;
    public settings = {
        stateImage: true,
        stateAnnotation: true,
        image: {
            control: true,
            zoomin: true,
            zoomout: true,
            fittowindow: true,
            rotateright: true,
            rotateleft: true,
            crop: true,
            magnify: true,
            saveas: true,
            copy: true,
            movedocument: true,
            add: true,
            deletedocument: true,
            email: true,
            fax: true,
            print: true,
            signature: true
        },
        annotation: {
            picturecontrol: true,
            showhide: true,
            addtext: true,
            addnote: true,
            addstamp: true,
            addhighlight: true,
            addredaction: true,
            addline: true,
            addarrow: true,
            filledrectangle: true,
            filledellipse: true,
            rectangle: true,
            ellipse: true
        },
        stickynote: {
            colorName: '#000000',
            fontFamily: 'Arial',
            fontSize: '14',
            fontWeight: false,
            fontStyle: false
        },
        textedit: {
            colorName: '#000000',
            fontFamily: 'Arial',
            fontSize: '14',
            fontWeight: false,
            fontStyle: false
        }
    };
    changeStateImageControl (event) {
        this.status_left = $(event.target).is(':checked');
        $('input[name="selected_image"]').prop('disabled', this.status_left ? '' : 'disabled');
        this.settings['image']['zoomin'] = this.status_left;
        this.settings['image']['zoomout'] = this.status_left;
        this.settings['image']['fittowindow'] = this.status_left;
        this.settings['image']['rotateright'] = this.status_left;
        this.settings['image']['rotateleft'] = this.status_left;
        this.settings['image']['crop'] = this.status_left;
        this.settings['image']['magnify'] = this.status_left;
        this.settings['image']['saveas'] = this.status_left;
        this.settings['image']['copy'] = this.status_left;
        this.settings['image']['move'] = this.status_left;
        this.settings['image']['add'] = this.status_left;
        this.settings['image']['delete'] = this.status_left;
        this.settings['image']['email'] = this.status_left;
        this.settings['image']['fax'] = this.status_left;
        this.settings['image']['print'] = this.status_left;
        this.settings['image']['signature'] = this.status_left;
    };

    changeStateAnnotationControl = function (event) {
        this.status_right = $(event.target).is(':checked');
        $('input[name="selected"]').prop('disabled', this.status_right ? '' : 'disabled');
        this.settings['annotation']['picturecontrol'] = this.status_right;
        this.settings['annotation']['showhide'] = this.status_right;
        this.settings['annotation']['addtext'] = this.status_right;
        this.settings['annotation']['addnote'] = this.status_right;
        this.settings['annotation']['addstamp'] = this.status_right;
        this.settings['annotation']['addhighlight'] = this.status_right;
        this.settings['annotation']['addredaction'] = this.status_right;
        this.settings['annotation']['addline'] = this.status_right;
        this.settings['annotation']['addarrow'] = this.status_right;
        this.settings['annotation']['filledrectangle'] = this.status_right;
        this.settings['annotation']['filledellipse'] = this.status_right;
        this.settings['annotation']['rectangle'] = this.status_right;
        this.settings['annotation']['ellipse'] = this.status_right;
    };
}
