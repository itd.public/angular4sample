import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentEditComponentComponent } from './document-edit-component.component';

describe('DocumentEditComponentComponent', () => {
  let component: DocumentEditComponentComponent;
  let fixture: ComponentFixture<DocumentEditComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentEditComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentEditComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
