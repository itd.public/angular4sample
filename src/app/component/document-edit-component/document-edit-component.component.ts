import { Component, ViewChild, AfterViewInit } from '@angular/core';

import { Config } from '../../app.config';
import { SettingModule } from '../../settings.module';
import { DocumentService } from '../../service/document-service.service';
import * as fs from 'fs';
import * as $ from 'jquery';

@Component({
  selector: 'app-document-edit-component',
  templateUrl: './document-edit-component.component.html',
  styleUrls: ['./document-edit-component.component.css']
})
export class DocumentEditComponentComponent implements AfterViewInit {
  private annotationsType: any[];
  private img = new Image();
  private documentPage: number = 1;
  private isMouseDown:boolean = false;
  private startX:number = 0;
  private startY:number = 0;
  private startMoveX:number = 0;
  private startMoveY:number = 0;
  private endX:number = 0;
  private endY:number = 0;
  private index:number = -1;
  private isMagnify:boolean = false;
  private isAddText:boolean = false;
  private isAddNote:boolean = false;
  private isAddStamp:boolean = false;
  private functionName:string = '';
  private listDocumentDemo: any;
  private listItemSelected: any;
  private fileNameDocument:string = '';
  private pathFolder:string = '';
  private rects: any[];
  private page:number = 1;
  private maxPages:number = 1;
  private defaultZoom:number = 1;
  private degrees:number = 0;
  private dragTL:boolean = false;
  private dragBL:boolean = false;
  private dragTR:boolean = false;
  private dragBR:boolean = false;
  private isDrag:boolean = false;
  private isDocumentFolder:boolean = false;
  private listRects: any[];
  private infoText: any[];
  private timeCreate: any;
  private listFolderOrDocument: any;
  private image = {
    brightness: 0,
    contrast: 0
  };

  context: CanvasRenderingContext2D;
  magnifyContext: CanvasRenderingContext2D;
  @ViewChild('magnify') magnify;

  constructor(private config: Config, private documentService: DocumentService, private setting: SettingModule) {
      this.annotationsType = this.documentService.getAnnotationsType(this.config.ANNOTATIONJSONURL);
      this.openFolder('src/data_test');
  }

  ngAfterViewInit() {
    const canvas: any = document.getElementById('page' + this.documentPage);
    if (canvas !== null) {
      const __this = this;
      const path = 'src/data_test/document';
      this.context = canvas.getContext('2d');
      this.listDocumentDemo = this.documentService.readAllFilesInDirectory(path);
      this.listDocumentDemo.forEach(function (value, key) {
          __this.listItemSelected.push({url: __this.convertUrl2Unit8Array(value.url)});
      });
      const annotation = this.documentService.getDataAnnotation(this.fileNameDocument, this.pathFolder + '/document');
      this.rects = annotation[0];
      this.maxPages = this.listItemSelected.length;
      const zoom = this.documentService.getZoomState(path, 'page_1.jpg', 1);
      const width = this.config.BASE_WIDTH * zoom;
      const height = this.config.BASE_HEIGHT * zoom;
      this.defaultZoom = zoom;
      this.reDrawImage(this.listItemSelected[0].url, width, height);
      this.pathFolder = path;
      this.fileNameDocument = 'page_1.jpg';
    }
  }

  openFolder(pathFolder: string) {
      const __this = this;
      if (!pathFolder.length) {
          const {dialog} = require('electron').remote;
          dialog.showOpenDialog({properties: ['openDirectory']}, function (path) {
              if (path !== undefined) {
                  __this.pathFolder = path[0];
                  __this.listFolderOrDocument = __this.documentService.readFolderAndDocument(path[0]);
              }
          });
      } else {
          __this.listFolderOrDocument = __this.documentService.readFolderAndDocument(pathFolder);
      }
  }

  onMouseDown(e) {
    if (e.button === 0) {
      this.isMouseDown = true;
      const pos = this.getMousePos(e, this.context);
      $('#popup').hide();
      const x = e.offsetX;
      const y = e.offsetY;
      this.startMoveX = pos.x;
      this.startMoveY = pos.y;
      this.dragTL = false;
      this.dragBL = false;
      this.dragTR = false;
      this.dragBR = false;
      this.index = -1;
      if (this.functionName === '') {
        if (this.rects !== undefined) {
          for (let i = 0; i < this.rects.length; i++) {
            const rec = this.rects[i];
            if (x > rec.x
              && x < rec.x + rec.width
              && y > rec.y
              && y < rec.y + rec.height
              && rec.pageNumber === this.documentPage) {
              this.listRects = [
                  [rec.x - 5, rec.y - 5, 10, 10, '#444444', this.documentPage],
                  [rec.x + rec.width - 5, rec.y - 5, 10, 10, '#444444', this.documentPage],
                  [rec.x + rec.width - 5, rec.y + rec.height - 5, 10, 10, '#444444', this.documentPage],
                  [rec.x - 5, rec.y + rec.height - 5, 10, 10, '#444444', this.documentPage]
              ];
              this.startX = rec.x;
              this.startY = rec.y;
              this.index = i;
              this.drawHandles(this.context, this.listRects, this.documentPage);
              this.isDrag = true;
            } else {
              if (this.index > -1) {
                this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
                this.context.drawImage(this.img, 0, 0, this.img.width, this.img.height,
                    0, 0, this.context.canvas.width, this.context.canvas.height);
                this.drawData();
              }
            }
          }
        }

        if (this.listRects !== undefined) {
          for (let i = 0; i < this.listRects.length; i++) {
            if (x > this.listRects[i][0]
              && x < this.listRects[i][0] + this.listRects[i][2]
              && y > this.listRects[i][1]
              && y < this.listRects[i][1] + this.listRects[i][3]
              && this.documentPage === this.listRects[i][5]) {
              if (i === 0) {
                  this.dragTL = true;
              } else if (i === 1) {
                  this.dragTR = true;
              } else if (i === 2) {
                  this.dragBR = true;
              } else if (i === 3) {
                  this.dragBL = true;
              }
            }
          }
        }
      }
      if (this.functionName !== '') {
        this.startX = this.endX = pos.x;
        this.startY = this.endY = pos.y;
        const w = this.endX - this.startX;
        const h = this.endY - this.startY;
        const width = Math.abs(w);
        const height = Math.abs(h);
        this.infoText = [];
        this.infoText = this.setInfoObject(this.functionName, this.documentPage);
        this.rects.push({
          state: this.functionName,
          info: this.infoText,
          x: this.startX,
          y: this.startY,
          width: width,
          height: height,
          pageNumber: this.documentPage
        });
        this.index = this.rects.length - 1;
        this.drawSquare(e, width, height);
      }
      switch (this.functionName) {
        // Add Note
        case this.config.listFunctionName.addNote:
          this.functionName = this.config.listFunctionName.addNote;
          break;
        // Add Text
        case this.config.listFunctionName.addText:
          this.functionName = this.config.listFunctionName.addText;
          break;
        default:
      }
    }
  }
  onMouseUp(e) {
    const $popupFormNote = $('#vvAnnPopUpForm');
    const $popupFormText = $('#vvAnnPopUpFormText');
    if (e.button === 0) {
      this.isMouseDown = false;
      this.dragTL = false;
      this.dragBL = false;
      this.dragTR = false;
      this.dragBR = false;
      this.isDrag = false;
      const pos = this.getMousePos(e, this.context);
      switch (this.functionName) {
        // Add Note
        case this.config.listFunctionName.addNote:
          this.addNoteFunction(e, $popupFormNote, pos);
          break;
        // Add Text
        case this.config.listFunctionName.addText:
          this.addTextFunction(e, $popupFormText, pos);
          break;
        default:
      }
      const data = [];
      if (this.index > -1) {
        const __this = this;
        if (this.rects[this.index].width <= 0 && this.rects[this.index].height <= 0) {
          this.rects.splice(this.index, 1);
          this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
          this.context.drawImage(this.img, 0, 0, this.img.width, this.img.height,
              0, 0, this.context.canvas.width, this.context.canvas.height);
          this.img.src = __this.listItemSelected[__this.documentPage - 1].url;
          __this.drawData();
        }
      }
      data.push(this.rects);
      this.documentService.saveDataToJsonFile(data, this.fileNameDocument, this.pathFolder);
      this.releaseState();
    }
    if (e.button === 2) {
      const x = e.offsetX;
      const y = e.offsetY;
      if (this.rects !== undefined) {
        for (let i = 0; i < this.rects.length; i++) {
          const rec = this.rects[i];
          if (x > rec.x
            && x < rec.x + rec.width
            && y > rec.y
            && y < rec.y + rec.height) {
            this.index = i;
            const state = this.rects[this.index].state;
            const functionName = this.config.listFunctionName;
            const popup = $('#popup');
            const wpopup = popup.width();
            const hpopup = popup.height();
            const $obj = $('#edit-document');
            const widthCanvas = $obj.width();
            const heightCanvas = $obj.height();
            const left = this.rects[this.index].x;
            let top = this.rects[this.index].y;
            if ((heightCanvas + top) >= 1450) {
              top = top - hpopup;
            }
            if (left <= wpopup) {
              const pos = this.rects[this.index].x + this.rects[this.index].width + 150;
              popup.css({'top': top, 'left': pos});
            } else {
              const pos = this.rects[this.index].x - this.rects[this.index].width - 50;
              popup.css({'top': top, 'left': pos});
            }
            popup.show();

            if (state === functionName.addText) {
              $popupFormNote.hide();
              $popupFormText.show();
              const info = this.rects[this.index].info[0];
              popup.find('#EditText').val(info.textData);
              popup.find('#EditText').css({
                'color': info.fontColor,
                'font-size': info.fontSize + 'px',
                'font-family': info.fontFamily,
                'font-weight': info.fontWeight,
                'font-style': info.fontStyle,
              });
              $('#vvAnnTextStyleItalic').prop('checked', (info.fontStyle === 'italic') ? 'checked' : '');
              $('#vvAnnTextStyleBold').prop('checked', (info.fontWeight === 'bold') ? 'checked' : '');
              $('#vvAnnTextFaceSelect').val(info.fontFamily);
              $('#vvAnnTextSize').val(info.fontSize);
              $('#fontColor').val(info.fontColor).css('background', info.fontColor);
              $('#popup .btn-select-color').css({'background': info.fontColor});
              $popupFormText.find('.annotation-index').val(this.index);
            } else if (state === functionName.addNote) {
              $popupFormNote.show();
              $popupFormText.hide();
              const info = this.rects[this.index].info[0];
              popup.find('#EditNote').val(info.textData);
              popup.find('#EditNote').css({
                'color': info.fontColor,
                'font-size': info.fontSize + 'px',
                'font-family': info.fontFamily,
                'font-weight': info.fontWeight,
                'font-style': info.fontStyle,
              });
              $('#vvAnnTextStyleItalicNote').prop('checked', (info.fontStyle === 'italic') ? 'checked' : '');
              $('#vvAnnTextStyleBoldNote').prop('checked', (info.fontWeight === 'bold') ? 'checked' : '');
              $('#vvAnnTextFaceSelectNote').val(info.fontFamily);
              $('#vvAnnTextSizeNote').val(info.fontSize);
              $('#fontColorNote').val(info.fontColor).css('background', info.fontColor);
              $('#popup .btn-select-color').css({'background': info.fontColor});
              $popupFormNote.find('.annotation-index').val(this.index);
            }
          }
        }
      }
    }
  }
  onMouseMove(e) {
    const pos = this.getMousePos(e, this.context);
    if (e.button === 0) {
      if (this.isMouseDown) {
        this.stopMagnify();
        if (this.isAddNote || this.isAddText) {
          this.endX = pos.x;
          this.endY = pos.y;
          const w = this.endX - this.startX;
          const h = this.endY - this.startY;
          const width = Math.abs(w);
          const height = Math.abs(h);
          this.drawSquare(e, width, height);
        } else {
          if (this.dragTL || this.dragTR || this.dragBL || this.dragBR) {
            const w = this.endX - this.startX;
            const h = this.endY - this.startY;
            const width = Math.abs(w);
            const height = Math.abs(h);
            this.drawSquare(e, width, height);
          } else if (this.isDrag) {
            const dx = pos.x - this.startMoveX;
            const dy = pos.y - this.startMoveY;
            this.startX += dx;
            this.startY += dy;
            const maxWidth = this.context.canvas.width - 5;
            const maxHeight = this.context.canvas.height - 5;
            if ((this.startX >= 5 && this.startY >= 5) &&
              (this.startY >= 5 && this.startX + this.rects[this.index].width <= maxWidth) &&
              (this.startX + this.rects[this.index].width <= maxWidth) &&
              (this.startY + this.rects[this.index].height <= maxHeight)) {
              this.drawSquare(e, this.rects[this.index].width, this.rects[this.index].height);
            }
            this.startMoveX = pos.x;
            this.startMoveY = pos.y;
          }
          this.drawHandles(this.context, this.listRects, this.documentPage);
        }
      }
    }
    if (this.isMagnify) {
      $('.magnify-glass').css('display', 'block');
      const magnifyCanvas = this.magnify.nativeElement;
      this.magnifyContext.fillStyle = 'white';
      this.magnifyContext.drawImage(this.context.canvas, pos.x, pos.y, this.context.canvas.width, this.context.canvas.height, 0, 0,
          this.context.canvas.width + (this.context.canvas.width / 2), this.context.canvas.height + (this.context.canvas.height / 2));
      magnifyCanvas.style.top = (e.pageY - this.magnifyContext.canvas.height) + 'px';
      magnifyCanvas.style.left = (e.pageX - this.magnifyContext.canvas.width - 300) + 'px';
      magnifyCanvas.style.display = 'block';
    }
  }
  drawSquare(e, width, height) {
    const pos = this.getMousePos(e, this.context);
    if (this.dragTL) {
      width += this.startX - pos.x;
      height += this.startY - pos.y;
      this.startX = pos.x;
      this.startY = pos.y;
    } else if (this.dragTR) {
      width = Math.abs(this.startX - pos.x);
      height += this.startY - pos.y;
      this.startY = pos.y;
      this.endX = width + this.startX;
    } else if (this.dragBL) {
      width += this.startX - pos.x;
      height = Math.abs(this.startY - pos.y);
      this.startX = pos.x;
      this.endX = width + this.startX;
      this.endY = height + this.startY;
    } else if (this.dragBR) {
      width = Math.abs(this.startX - pos.x);
      height = Math.abs(this.startY - pos.y);
      this.endX = width + this.startX;
      this.endY = height + this.startY;
    }
    const rectIndex = this.rects[this.index];
    if (this.index > -1 && this.rects.length) {
      this.rects[this.index] = {
        state: rectIndex.state,
        info: rectIndex.info,
        x: this.startX,
        y: this.startY,
        width: width,
        height: height,
        pageNumber: this.documentPage
      };
      if (this.functionName === this.config.listFunctionName.addStamp) {
        this.listRects = [];
      } else {
        this.listRects = [
          [rectIndex.x - 5, rectIndex.y - 5, 10, 10, '#444444', this.documentPage],
          [rectIndex.x + rectIndex.width - 5, rectIndex.y - 5, 10, 10, '#444444', this.documentPage],
          [rectIndex.x + rectIndex.width - 5, rectIndex.y + rectIndex.height - 5, 10, 10, '#444444', this.documentPage],
          [rectIndex.x - 5, rectIndex.y + rectIndex.height - 5, 10, 10, '#444444', this.documentPage]
        ];
      }
    }
    this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    const zoom = this.documentService.getZoomState(this.pathFolder, this.fileNameDocument, this.documentPage);
    this.context.canvas.width = this.config.BASE_WIDTH * zoom;
    this.context.canvas.height = this.config.BASE_HEIGHT * zoom;
    this.defaultZoom = zoom;
    this.img.src = this.listItemSelected[this.documentPage - 1].url;
    this.context.drawImage(this.img, 0, 0, this.img.width, this.img.height,
        0, 0, this.context.canvas.width, this.context.canvas.height);
    this.drawData();
  }

  changeDocument(id) {
    this.documentPage = id;
    this.page = id;
    this.defaultZoom = 1;
    this.activeThumb(id);
    const zoom = this.documentService.getZoomState(this.pathFolder, this.fileNameDocument, this.documentPage);
    const width = this.config.BASE_WIDTH * zoom;
    const height = this.config.BASE_HEIGHT * zoom;
    this.defaultZoom = zoom;
    this.reDrawImage(this.listItemSelected[this.documentPage - 1].url, width, height);
  }

  showDocument(path, fileName, isDocumentFolder) {
    this.releaseData();
    this.listItemSelected = [];
    this.isDocumentFolder = true;
    this.listDocumentDemo = this.documentService.readAllFilesInDirectory(path);
    this.fileNameDocument = this.listDocumentDemo[0].fileName;
    this.pathFolder = path;
    const __this = this;
    this.listDocumentDemo.forEach(function (value, key) {
        __this.listItemSelected.push({url: __this.convertUrl2Unit8Array(value.url)});
    });
    this.maxPages = this.listItemSelected.length;
    const zoom = this.documentService.getZoomState(this.pathFolder, this.fileNameDocument, this.documentPage);
    const width = this.config.BASE_WIDTH * zoom;
    const height = this.config.BASE_HEIGHT * zoom;
    this.defaultZoom = zoom;
    this.reDrawImage(this.listItemSelected[0].url, width, height);
  }

  addNoteFunction(e, $popup, pos) {
    this.endX = pos.x;
    this.endY = pos.y;
    if (this.endX !== this.startX && this.endY !== this.startY) {
      const w = this.endX - this.startX;
      const h = this.endY - this.startY;
      const width = Math.abs(w);
      const height = Math.abs(h);
      this.drawSquare(e, width, height);
      this.isAddNote = false;
      this.functionName = '';
      this.setCursor(this.isAddNote);
      $popup.show();
    }
  }
  addTextFunction(e, $popup, pos) {
    this.endX = pos.x;
    this.endY = pos.y;
    if (this.endX !== this.startX && this.endY !== this.startY) {
      const w = this.endX - this.startX;
      const h = this.endY - this.startY;
      const width = Math.abs(w);
      const height = Math.abs(h);
      this.drawSquare(e, width, height);
      this.isAddText = false;
      this.functionName = '';
      this.setCursor(this.isAddText);
      this.timeCreate = new Date();
      $popup.show();
    }
  }
  reDrawImage(src: string, ...data) {
    const __this = this;
    const image = new Image();
    image.onload = function () {
        const annotation = __this.documentService.getDataAnnotation(__this.fileNameDocument, __this.pathFolder);
        __this.rects = annotation[0];
        __this.degrees = __this.documentService.getRotateObject(__this.fileNameDocument, __this.pathFolder);
        const canvas: any = document.getElementById('page' + __this.documentPage);
        __this.context = canvas.getContext('2d');
        const rotation = __this.degrees % 360;
        let w = __this.config.BASE_WIDTH;
        let h = __this.config.BASE_HEIGHT;
        if (data[0] !== undefined) {
            w = data[0];
        }
        if (data[1] !== undefined) {
            h = data[1];
        }
        if (rotation === 90 || rotation === -270 || rotation === -90 || rotation === 270) {
            $('#page' + __this.documentPage).css({'width': h + 'px', 'height': w + 'px'});
            __this.context.canvas.width = canvas.width = h;
            __this.context.canvas.height = canvas.height = h;
        } else {
            $('#page' + __this.documentPage).css({'width': w + 'px', 'height': h + 'px'});
            __this.context.canvas.width = canvas.width = w;
            __this.context.canvas.height = canvas.height = h;
        }
        __this.context.clearRect(0, 0, image.width, image.height);
        __this.context.clearRect(0, 0, __this.context.canvas.width, __this.context.canvas.height);
        __this.DrawRotate(__this.context, image, rotation, __this.image.brightness, __this.image.contrast);

        setTimeout(function () {
            __this.drawData();
            $('.box-loading2').addClass('hidden');
        }, 100);
    };
    image.src = src;
    __this.img.src = src;
  }
  DrawRotate(context, image, ...params) {
      const degree = params[0];
      const percent = (((params[1] - -145) * 100) / 232) * 1.7;
      let percentContrast = 100;
      let valC = params[2];
      if (valC > 0) {
          valC *= 3;
      }
      percentContrast = (((valC - -135) * 100) / 220) * 1.7;
      const canvas = context.canvas;
      const tmpWidth = canvas.width;
      const tmpHeight = canvas.height;
      if (degree === 90 || degree === 270 || degree === -90 || degree === -270) {
          canvas.width = tmpHeight;
          canvas.height = tmpWidth;
      } else {
          canvas.width = tmpWidth;
          canvas.height = tmpHeight;
      }
      context.clearRect(0, 0, image.width, image.height);
      context.filter = 'brightness(' + Math.round(percent) + '%) contrast(' + Math.round(percentContrast) + '%)';
      context.rotate(degree * Math.PI / 180);
      if (degree === 90 || degree === -270) {
          context.translate(0, - canvas.width);
          context.drawImage(image, 0, 0, image.width, image.height,
              0, 0, canvas.height, canvas.width);
      } else if (degree === 180 || degree === -180) {
          context.translate(-canvas.width, -canvas.height);
          context.drawImage(image, 0, 0, image.width, image.height,
              0, 0, canvas.width, canvas.height);
      } else if (degree === -90 || degree === 270) {
          context.translate(-canvas.height, 0);
          context.drawImage(image, 0, 0, image.width, image.height,
              0, 0, canvas.height, canvas.width);
      } else if (degree === 0 || degree === 360  || degree === -360 ) {
          context.translate(0, 0);
          context.drawImage(image, 0, 0, image.width, image.height,
              0, 0, canvas.width, canvas.height);
      }
  };
  drawData() {
    if (this.rects.length) {
      this.drawRectangle(this.context, this.rects, this.documentPage);
    }
  }
  drawRectangle(context, rects, pageNumber) {
    if (rects.length) {
      for (let i = 0; i < rects.length; i++) {
        const rec = rects[i];
        if ((rec.state !== this.config.listFunctionName.addStamp && rec.pageNumber === pageNumber) &&
            (rec.state !== this.config.listFunctionName.addLine && rec.pageNumber === pageNumber)) {
            context.beginPath();
            context.rect(rec.x, rec.y, rec.width, rec.height);
            context.stroke();
        }
        if (rec.state === this.config.listFunctionName.addNote && rec.pageNumber === pageNumber) {
            context.fillStyle = rec.info[0].fillColor;
            context.fillRect(rec.x, rec.y, rec.width, rec.height);
            context.font = rec.info[0].fontStyle + ' ' + rec.info[0].fontWeight + ' ' +
                rec.info[0].fontSize + 'px ' + rec.info[0].fontFamily;
            context.fillStyle = rec.info[0].fontColor;
            const text = rec.info[0].textData;
            if(rec.info[0].fontSize === '18'){
                this.wrapText(context, text, rec.x + 5, rec.y + 25, rec.width - 5, 20);
            } else if(rec.info[0].fontSize === '24'){
                this.wrapText(context, text, rec.x + 5, rec.y + 30, rec.width - 5, 30);
            } else if(rec.info[0].fontSize === '38'){
                this.wrapText(context, text, rec.x + 5, rec.y + 35, rec.width - 5, 40);
            } else {
                this.wrapText(context, text, rec.x + 5, rec.y + 15, rec.width - 5, 20);
            }
        } else if (rec.state === this.config.listFunctionName.addText && rec.pageNumber === pageNumber) {
          context.font = rec.info[0].fontStyle + ' ' + rec.info[0].fontWeight + ' ' +
              rec.info[0].fontSize + 'px ' + rec.info[0].fontFamily;
          context.fillStyle = rec.info[0].fontColor;
          const text = rec.info[0].textData;
          if(rec.info[0].fontSize === '18'){
              this.wrapText(context, text, rec.x + 5, rec.y + 25, rec.width - 5, 20);
          } else if(rec.info[0].fontSize === '24'){
              this.wrapText(context, text, rec.x + 5, rec.y + 30, rec.width - 5, 30);
          } else if(rec.info[0].fontSize === '38'){
              this.wrapText(context, text, rec.x + 5, rec.y + 35, rec.width - 5, 40);
          } else {
              this.wrapText(context, text, rec.x + 5, rec.y + 15, rec.width - 5, 20);
          }
        }
      }
    }
  }
  wrapText(context, text, x, y, maxWidth, lineHeight) {
    const words = text.split('');
    let line = '';

    for (let n = 0; n < words.length; n++) {
      const testLine = line + words[n];
      const metrics = context.measureText(testLine);
      const testWidth = metrics.width;
      if (testWidth > maxWidth && n > 0) {
        context.fillText(line, x, y);
        line = words[n];
        y += lineHeight;
      } else {
        line = testLine;
      }
    }
    context.fillText(line, x, y);
  }
  convertUrl2Unit8Array(path: string) {
      return URL.createObjectURL(new Blob([new Uint8Array(fs.readFileSync(path))]));
  }
  getMousePos(evt, context) {
      const rect = context.canvas.getBoundingClientRect();
      return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
      };
  }
  drawHandles(context, listRects, pageNumber) {
    if (listRects !== undefined) {
      for (let i = 0; i < listRects.length; i++) {
        if (pageNumber === listRects[i][5]) {
          context.fillStyle = listRects[i][4];
          context.fillRect(listRects[i][0], listRects[i][1], listRects[i][2], listRects[i][3]);
        }
      }
    }
  }
  setInfoObject(functionName: string, pageNumber: number) {
    const object = [];
    if (functionName === this.config.listFunctionName.addNote) {
      const sticky = this.setting.settings.stickynote;
      object.push({
        textData: 'Text',
        fontSize: sticky.fontSize,
        fontFamily: sticky.fontFamily,
        fontColor: sticky.colorName,
        fontWeight: sticky.fontWeight ? 'bold' : '',
        fontStyle: sticky.fontStyle ? 'italic' : '',
        fillColor: this.config.LIST_COLOR_FILL[functionName],
        pageNumber: pageNumber
      });
    } else if (functionName === this.config.listFunctionName.addText) {
      const textedit = this.setting.settings.textedit;
      object.push({
        textData: 'Text',
        fontSize: textedit.fontSize,
        fontFamily: textedit.fontFamily,
        fontColor: textedit.colorName,
        fontWeight: textedit.fontWeight ? 'bold' : '',
        fontStyle: textedit.fontStyle ? 'italic' : '',
        fillColor: this.config.LIST_COLOR_FILL[functionName],
        pageNumber: pageNumber
      });
    }
    return object;
  }
  releaseState() {
    this.isMagnify = false;
    this.isAddText = false;
    this.isAddNote = false;
    this.isAddStamp = false;
    this.index = -1;
  }
  stopMagnify() {
    $('.magnify-glass').css('display', 'none');
  }
  setCursor(functionSelected: boolean) {
    $('canvas').css('cursor', functionSelected ? 'crosshair' : 'default');
  }
  releaseData() {
    this.maxPages = 1;
    this.documentPage = 1;
    this.listItemSelected = [];
  }
  activeThumb(id:number) {
    const obj = $('.document-thumb-' + id + ' img.img-responsive');
    obj.removeClass('active');
    $('img.img-responsive').removeClass('active');
    obj.addClass('active');
  }
}
