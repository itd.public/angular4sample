import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { DocumentEditComponentComponent } from './component/document-edit-component/document-edit-component.component';
import { Config } from './app.config';
import { SettingModule } from './settings.module';
import { DocumentService } from './service/document-service.service';

@NgModule({
  declarations: [
    AppComponent,
    DocumentEditComponentComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot()
  ],
  providers: [
    DocumentService,
    SettingModule,
    Config
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
