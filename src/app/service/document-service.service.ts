import { Injectable } from '@angular/core';

import * as fs from 'fs';
import { Config } from '../app.config';

@Injectable()
export class DocumentService {
  private array_ext = ['jpg', 'png', 'jpeg', 'JPG', 'PNG', 'JPEG'];

  constructor(private config: Config) { }

  readFolderAndDocument(path) {
    const files = fs.readdirSync(path);
    const result = [];
    const dataFiles = [];
    const arrayAnnotation = [];
    const __this = this;
    const fileJsonInput = path + '/' + __this.config.JSON_INPUT;
    let AppData = '/AppData';
    require('dotenv').config();
    const dir = __dirname.replace('node_modules\\electron-prebuilt\\dist\\resources\\electron.asar\\renderer', '');
    AppData = dir + AppData;
    if (fs.existsSync(AppData)) {
      const listFiles = fs.readdirSync(AppData);
      for (const file of listFiles) {
        const newPath = AppData + '/' + file;
        fs.unlink(newPath, err => {
          if (err) {
            throw err;
          }
        });
      }
    } else {
      fs.mkdir(AppData);
    }
    if (!fs.existsSync(fileJsonInput)) {
      files.forEach(function (fileName) {
        const docFile = path + '/' + fileName;
        const ext = fileName.split('.');
        const stat = fs.statSync(docFile);
        if (!(stat && stat.isDirectory())) {
          if (__this.array_ext.indexOf(ext[ext.length - 1]) >= 0) {
            dataFiles.push({
              'url': __this.replaceAll(docFile, '\\\\', '/'),
              'fileName': fileName,
              'degree': 0,
              'zoom': []
            });
            const data = [];
            data.push([]);
            arrayAnnotation.push({'fileName': fileName, 'data': data});
          }
        }
      });
      const tmpData = [{
          'listFile': dataFiles,
          'annotation': arrayAnnotation
      }];
      fs.writeFileSync(fileJsonInput, JSON.stringify(tmpData), 'utf8');
    } else {
        const json = JSON.parse(fs.readFileSync(fileJsonInput, 'utf8'));
    }
    files.forEach(function (value) {
      const file = path + '/' + value;
      const stat = fs.statSync(file);
      const icon = [
        'icon_jpg.png',
        'icon_png.png',
        'icon_jpg.png',
        'icon_jpg.png',
        'icon_png.png',
        'icon_jpg.png'];
      if (stat) {
        if (stat.isDirectory()) {
          let isDocumentFolder = false;
          isDocumentFolder = value.indexOf('document') > -1;
          const data = {
            path: __this.replaceAll(file, '\\\\', '/'),
            'fileName': value,
            'isDirectory': true,
            'isDocumentFolder': isDocumentFolder,
            'icon': 'icon_folder.png',
            'ext': ''
          };
          result.push(data);
        } else {
          const ext = value.split('.');
          const index = __this.array_ext.indexOf(ext[ext.length - 1]);
          if (index !== -1) {
            let url = __this.replaceAll(file, '\\\\', '/');
            const data = {
                path: url,
                'fileName': value,
                'isDirectory': false,
                'isDocumentFolder': false,
                'icon': icon[index],
                'ext': __this.array_ext[index]
            };
            result.push(data);
          }
        }
      }
    });
    return result;
  }

  getAnnotationsType(url: string) {
      return JSON.parse(fs.readFileSync(url, 'utf8'));
  }

  readAllFilesInDirectory(dir:string) {
    const result = [];
    const files = fs.readdirSync(dir);
    const __this = this;
    const fileJsonInput = dir + '/' + __this.config.JSON_INPUT;
    const dataFiles = [];
    const arrayAnnotation = [];
    if (!fs.existsSync(fileJsonInput)) {
      files.forEach(function (fileName) {
        const docFile = dir + '/' + fileName;
        const ext = fileName.split('.');
        const stat = fs.statSync(docFile);
        if (stat) {
          if (!stat.isDirectory()) {
            const index = __this.array_ext.indexOf(ext[ext.length - 1]);
            if (index !== -1) {
              dataFiles.push({
                'url': __this.replaceAll(docFile, '\\\\', '/'),
                'fileName': fileName,
                'degree': 0,
                'zoom': []
              });
              const data = [];
              data.push([]);
              arrayAnnotation.push({'fileName': fileName, 'data': data});
            }
          }
        }
      });
      const tmpData = [{
        'listFile': dataFiles,
        'annotation': arrayAnnotation
      }];
      fs.writeFileSync(fileJsonInput, JSON.stringify(tmpData), 'utf8');
    }
    files.forEach(function (value) {
      const file = dir + '/' + value;
      const stat = fs.statSync(file);
      if (stat) {
        if (!stat.isDirectory()) {
          const ext = value.split('.');
          const index = __this.array_ext.indexOf(ext[ext.length - 1]);
          if (index !== -1) {
            const data = {
              'url': __this.replaceAll(file, '\\\\', '/'),
              'fileName': value
            };
            result.push(data);
          }
        }
      }
    });
    return result;
  }

  getDataAnnotation(fileName:string, pathFolder:string) {
    const file = pathFolder + '/' + this.config.JSON_INPUT;
    let results = [];
    const json = JSON.parse(fs.readFileSync(file, 'utf8'));
    results = json[0].annotation;
    json[0].annotation.forEach(function (value, key) {
      if (value.fileName === fileName) {
        results = json[0].annotation[key].data;
      }
    });
    return results;
  }

  getZoomState(pathFolder:string, fileName:string, pageNumber:number) {
    const file = pathFolder + '/' + this.config.JSON_INPUT;
    let results = 1;
    const json = JSON.parse(fs.readFileSync(file, 'utf8'));
    json[0].listFile.forEach(function (value, key) {
        if (value.fileName === fileName) {
            json[0].listFile[key].zoom.forEach(function (v, k) {
                if (v.pageNumber === pageNumber) {
                    results = json[0].listFile[key].zoom[k].zoom;
                }
            });
        }
    });
    return results;
  }

  getRotateObject(fileName:string, pathFolder:string) {
    const file = pathFolder + '/' + this.config.JSON_INPUT;
    let results = 0;
    const json = JSON.parse(fs.readFileSync(file, 'utf8'));
    json[0].listFile.forEach(function (value, key) {
      if (value.fileName === fileName) {
          results = json[0].listFile[key].degree;
      }
    });
    return results;
  }

  saveDataToJsonFile(data, fileName, pathFolder) {
    const file = pathFolder + '/' + this.config.JSON_INPUT;
    const json = JSON.parse(fs.readFileSync(file, 'utf8'));
    json[0].annotation.forEach(function (value, key) {
      if (value.fileName === fileName) {
        json[0].annotation[key].data = data;
      }
    });
    const tmpData = [{
      'listFile': json[0].listFile,
      'annotation': json[0].annotation
    }];
    fs.writeFileSync(file, JSON.stringify(tmpData), 'utf8');
  }

  replaceAll(str:string, find:string, replace:string) {
      return str.replace(new RegExp(find, 'g'), replace);
  }
}
