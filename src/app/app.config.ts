/**
 * Created by Kiem on 16/10/2017.
 */
import {Injectable} from '@angular/core';

@Injectable()
export class Config {
    ANNOTATIONJSONURL = 'src/assets/data/annotations.json';
    JSON_INPUT = 'data.json';
    BASE_WIDTH = 720;
    BASE_HEIGHT = 932;
    listFunctionName = {
        crop: 'crop',
        addText: 'addText',
        addNote: 'addNote',
        addStamp: 'addStamp',
        addHighlightRectangle: 'addHighlightRectangle',
        addRedactionArea: 'addRedactionArea',
        addLine: 'addLine',
        addArrow: 'addArrow',
        addFilledRectangle: 'addFilledRectangle',
        addFilledEllipse: 'addFilledEllipse',
        addFilledPolygon: 'addFilledPolygon',
        addRectangle: 'addRectangle',
        addEllipse: 'addEllipse',
        addPolygon: 'addPolygon'
    };
    STYLE_TEXT = {
        fontColor: '#000000',
        fontFamily: 'Arial',
        fontSize: '14px',
        fontWeight: 'normal',
        fontStyle: 'normal'
    };
    LIST_COLOR_FILL = {
        addHighlightRectangle: 'rgba(255, 0, 0, 0.35)',
        addRedactionArea: 'rgba(0, 0, 0, 1)',
        addFilledRectangle: 'rgba(255, 0, 0, 1)',
        addRectangle: 'rgba(255, 0, 0, 0)',
        addEllipse: 'rgba(255, 0, 0, 0)',
        addFilledEllipse: 'rgba(255, 0, 0, 1)',
        addNote: 'rgb(254, 238, 163)',
        addText: 'transparent'
    };
    LIST_LINE_SIZE = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    LIST_FONT_SIZE = [8, 9, 10, 12, 14, 18, 24, 38];
    LIST_FONT_FAMILY = [
        'Helvetica',
        'Times New Roman',
        'Arial',
        'Courier',
        'Courier New'
    ];
    LIST_FONT_COLOR = [
        'rgb(255, 255, 255)',
        'rgb(0, 0, 0)',
        'rgb(192, 192, 192)',
        'rgb(0, 128, 0)',
        'rgb(254, 0, 0)',
        'rgb(255, 117, 24)',
        'rgb(255, 255, 0)',
        'rgb(0, 0, 255)',
        'rgb(128, 128, 0)',
        'rgb(128, 128, 128)',
        'rgb(255, 0, 255)',
        'rgb(128, 0, 128)',
        'rgb(128, 0, 0)',
        'rgb(0, 255, 33)',
        'rgb(255, 216, 0)',
        'rgb(0, 0, 128)',
        'rgb(0, 128, 128)',
        'rgb(0, 255, 255)',
        'rgb(210, 180, 140)',
        'rgb(245, 245, 220)',
        'rgb(115, 194, 251)',
        'rgb(152, 251, 152)',
        'rgb(220, 208, 255)',
        'rgb(253, 253, 150)'
    ];
    LIST_MESSAGE = [
        'Continue without saving changes?',
        'Successfully saved stamp',
        'Please fill full textbox',
        'Successfully saved note',
        'Successfully saved text',
        'Successfully removed ',
        'Do you want to save your changes?',
        'Successfully saved update',
        'Error save image',
        'Are you sure destroy crop?',
        'Please fill form in correct format!',
        'Successfully saved field value',
        'Do you want delete this file?',
        'Successfully saved file ',
        'Successfully copy file ',
        'Successfully move file ',
        'You cannot access the server at this time please try again later or contact your system administrator',
        'You have not added a title or content for this document. Do you still want to send this email?',
        'You must enter page number between 1 and ',
        'Please collapse before sort field',
        'Successfully update field',
        'This document has annotations and Show Annotations is turned off.\nIf you want annotations to print turn on Show Annotations',
        'Please enter your fax number.',
        'Please select at least 1 email address.',
        'Only jpg, png, pdf, tiff are allowed.',
        'Add Signature Successfully'
    ];
}

